'use strict'

const lockfile = require('proper-lockfile');
const fs = require('fs');
const crypto = require('crypto');
const del = require('del');
const uuidv4 = require('uuid/v4');
const semver = require('semver');
const workerId = uuidv4();
const engines = require('./package').engines;

let cfg = { shardShares: Number.MAX_SAFE_INTEGER };

let myTasks = new Map();
let monitoredTasks = new Set();
let currentWorkerInfo = { setHash: "", alive: 0, livingSet: [], lastChage: new Date() };
let myShardRange;

const logger = global.logger ? global.logger : console;

// TODO: what happens if I lose access to the FS? 
// my lock will become stale and someone else will take it. Will I know about it and stop working on it?
function getNewShardIndex() {
    return Math.floor(Math.random() * (cfg.shardShares - 1));
}
function getMyShardRange() {
    let switchTolerance = new Date(new Date().getTime() - cfg.pauseAfterWorkersetChange);
    if (currentWorkerInfo.lastChage > switchTolerance) {
        return null;
    }
    return myShardRange;
}
function updateMyShardRange() {
    let livingWorkers = currentWorkerInfo.livingSet;
    let shares = cfg.shardShares / livingWorkers.length;
    for (let i = 0; i < livingWorkers.length; i++) {
        if (livingWorkers[i] === workerId.toString()) {
            myShardRange = { first: Math.floor(i * shares), last: Math.floor((i + 1) * shares - 1) };
            break;
        }
    }
}
function isTaskMine(taskId) {
    monitoredTasks.add(taskId);
    if (!myTasks.has(taskId)) {
        return false;
    }
    let switchTolerance = new Date(new Date().getTime() - cfg.pauseAfterWorkersetChange);
    return myTasks.get(taskId).acquiredAt < switchTolerance;
}
async function checkState() {
    for (let task of monitoredTasks) {
        try {
            if (!myTasks.has(task)) {
                if (await tryTaskOwnership(task)) {
                    break; // don't grab all tasks at once, give a chance to other nodes
                }
            }
        } catch (err) {
            logger.error(err);
        }
    }
    let workerInfo = await getWorkerInfo();
    if (workerInfo && currentWorkerInfo.setHash !== workerInfo.setHash) {
        for (let myTask of myTasks) {
            myTask[1].release();
            myTasks.delete(myTask[0]);
        }
        currentWorkerInfo = workerInfo;
        currentWorkerInfo.lastChage = new Date();
        updateMyShardRange();
    }
    setTimeout(checkState, cfg.checkInterval);
}
async function tryTaskOwnership(task) {
    let lockPath = `${cfg.sharedDirectory}/tasks/${task}`;
    try {
        let taskRelease = await lockfile.lock(lockPath, { stale: cfg.heartbeatTimeout, update: cfg.heartbeatInterval, realpath: false });
        myTasks.set(task, { release: taskRelease, acquiredAt: new Date() });
        return true;
    } catch (err) {
        if (err.code !== 'ELOCKED') {
            logger.error(err);
        }
    }
    return false;
}
async function getWorkerInfo() {
    try {
        let items = fs.readdirSync(`${cfg.sharedDirectory}/task-workers`);
        let livingWorkers = [];
        for (let item of items) {
            let stats = fs.statSync(`${cfg.sharedDirectory}/task-workers/${item}`);
            let tolerance = new Date().getTime() - cfg.heartbeatTimeout;
            // ignore normal files
            if (stats.isDirectory()) {
                if (stats.mtimeMs > tolerance) {
                    livingWorkers.push(item);
                } else {
                    del.sync([`${cfg.sharedDirectory}/task-workers/${item}`], { force: true });
                }
            }
        }
        let hash = crypto.createHash('sha256').update(JSON.stringify(livingWorkers)).digest('hex');
        let workerInfo = {
            alive: livingWorkers.length,
            setHash: hash,
            livingSet: livingWorkers
        }
        return workerInfo;
    } catch (err) {
        logger.error(err);
    }
}
async function beatMyHeart() {
    try {
        // check how proper-lockfile updates mtime
        del.sync([`${cfg.sharedDirectory}/task-workers/${workerId}/*`], { force: true });
        fs.mkdirSync(`${cfg.sharedDirectory}/task-workers/${workerId}/heartbeat`, { recursive: true });
    } catch (error) {
        logger.log(`Failure heartbeating: ${error}`);
    } finally {
        setTimeout(beatMyHeart, cfg.heartbeatInterval);
    }
}
async function init() {
    // check node version
    const version = engines.node;
    if (!semver.satisfies(process.version, version)) {
        console.log(`Required node version ${version} not satisfied with current version ${process.version}.`);
        process.exit(1);
    }
    // make sure essential directories exist
    try {
        fs.mkdirSync(`${cfg.sharedDirectory}/task-workers`, { recursive: true });
        fs.mkdirSync(`${cfg.sharedDirectory}/tasks`, { recursive: true });
    } catch (err) {
        if (err.code !== 'EEXIST') {
            logger.error(err);
        }
    }
}
// random start minimize chance of race
let startDelay = Math.floor(Math.random() * 1000)
async function join({
    sharedDirectory = '/tmp/cluster',
    checkInterval = 1000,
    heartbeatInterval = 500,
    heartbeatTimeout = 2000,
    pauseAfterWorkersetChange = 5000 } = {}) {
    cfg.sharedDirectory = sharedDirectory;
    cfg.checkInterval = checkInterval;
    cfg.heartbeatInterval = heartbeatInterval;
    cfg.heartbeatTimeout = heartbeatTimeout;
    cfg.pauseAfterWorkersetChange = pauseAfterWorkersetChange;
    await init();
    setTimeout(checkState, startDelay);
    setTimeout(beatMyHeart, cfg.heartbeatInterval);
}

module.exports = {
    isTaskMine,
    getNewShardIndex,
    getMyShardRange,
    join
}